/**
 * Created by Andrew on 2/06/2016.
 */

package com.apps.pittstreet.vrpittstreet.sensors;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import com.apps.pittstreet.vrpittstreet.MainActivity;

public class BearingSensor extends Activity implements SensorEventListener {

    private MainActivity mainActivity;
    private SensorManager sensorManager;
    private Sensor rotationSensor;

    private float[] rotVec;

    public BearingSensor(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        sensorManager = (SensorManager)mainActivity.getSystemService(SENSOR_SERVICE);
        rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
    }

    public void startSensing() {
        sensorManager.registerListener(this, rotationSensor, SensorManager.SENSOR_DELAY_GAME);
    }

    public void stopSensing() {
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            rotVec = event.values;
        }

        if(rotVec != null) {
            float[] rotationMatrix = new float[9];
            float[] remap = new float[9];
            SensorManager.getRotationMatrixFromVector(rotationMatrix, rotVec);
            SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_X, SensorManager.AXIS_Z, remap);

            float[] orientation = new float[3];
            SensorManager.getOrientation(remap, orientation);
            float azimuth = (float)Math.toDegrees(orientation[0]);
            float tilt = (float)Math.toDegrees(orientation[1]);
            azimuth = (azimuth < 0) ? azimuth + 360 : azimuth;

            mainActivity.changeBearing(azimuth, -tilt);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}

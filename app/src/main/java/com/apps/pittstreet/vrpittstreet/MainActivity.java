/**
 * Created by Andrew on 2/06/2016.
 */
package com.apps.pittstreet.vrpittstreet;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.apps.pittstreet.vrpittstreet.Task.DataServerAsyncTask;
import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.google.vr.sdk.widgets.pano.VrPanoramaView.Options;

import com.apps.pittstreet.vrpittstreet.core.MainProcess;
import com.apps.pittstreet.vrpittstreet.sensors.BearingSensor;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewPanoramaLink;
import com.google.android.gms.maps.model.StreetViewPanoramaLocation;

public class MainActivity extends AppCompatActivity {

    private static final LatLng WAIKATO_UNI = new LatLng(-37.787603, 175.311832);
    private static final LatLng PARIS = new LatLng(48.857825, 2.295183);

    private VrPanoramaView panoWidgetView;
    private Options panoOptions = new Options();

    private StreetViewPanorama streetViewPanorama;
    private BearingSensor bearing;
    private MainProcess mainProcess;
    private DataServerAsyncTask _mDataTask;
    private boolean runMainProcess;
    private boolean walkEnabled;
    private boolean isMove;
    private String nextPanoID;
    public boolean _Step = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        runMainProcess = true;
        mainProcess = new MainProcess(this);
        mainProcess.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        _mDataTask = new DataServerAsyncTask(MainActivity.this);
        _mDataTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final MainActivity thisActivity = this;

        panoOptions = new Options();
        panoOptions.inputType = Options.TYPE_MONO;
        panoWidgetView = (VrPanoramaView) findViewById(R.id.pano_view);
        panoWidgetView.setEventListener(new ActivityEventListener());
        panoWidgetView.setFullscreenButtonEnabled(false);
        panoWidgetView.setInfoButtonEnabled(false);
        panoWidgetView.setVrModeButtonEnabled(false);

        SupportStreetViewPanoramaFragment streetViewPanoramaFragment =
                (SupportStreetViewPanoramaFragment)
                        getSupportFragmentManager().findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                new OnStreetViewPanoramaReadyCallback() {
                    @Override
                    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
                        streetViewPanorama = panorama;
                        streetViewPanorama.setPosition(PARIS);
                        streetViewPanorama.setPanningGesturesEnabled(false);
                        streetViewPanorama.setUserNavigationEnabled(false);
                        streetViewPanorama.setStreetNamesEnabled(false);
                        bearing = new BearingSensor(thisActivity);
                        bearing.startSensing();
                        walkEnabled = true;
                        isMove = false;
                    }
                });
    }

    protected void onDestroy() {
        Log.i("DESTROY", "Destroyed!");
        runMainProcess = false;
        panoWidgetView.shutdown();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        panoWidgetView.pauseRendering();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        panoWidgetView.resumeRendering();
    }

    public void onMovePosition(View view) {
        movePosition();
    }

    public void movePosition() {
        if(walkEnabled) {
            StreetViewPanoramaLocation location = streetViewPanorama.getLocation();
            StreetViewPanoramaCamera camera = streetViewPanorama.getPanoramaCamera();
            if (location != null && location.links != null) {
                StreetViewPanoramaLink link = findClosestLinkToBearing(location.links, camera.bearing);
                move(link.panoId);
            }
        }
    }

    private synchronized void move(String panoID) {
        walkEnabled = false;
        streetViewPanorama.setPosition(panoID);
        nextPanoID = panoID;
        isMove = true;
    }

    public void updateVR(Bitmap image) {
        Log.d("VR", "Loading image into pano");
        panoWidgetView.loadImageFromBitmap(image, panoOptions);
    }

    public static StreetViewPanoramaLink findClosestLinkToBearing(StreetViewPanoramaLink[] links, float bearing) {
        float minBearingDiff = 360;
        StreetViewPanoramaLink closestLink = links[0];
        for (StreetViewPanoramaLink link : links) {
            if (minBearingDiff > findNormalizedDifference(bearing, link.bearing)) {
                minBearingDiff = findNormalizedDifference(bearing, link.bearing);
                closestLink = link;
            }
        }
        return closestLink;
    }

    // Find the difference between angle a and b as a value between 0 and 180
    public static float findNormalizedDifference(float a, float b) {
        float diff = a - b;
        float normalizedDiff = diff - (float) (360 * Math.floor(diff / 360.0f));
        return (normalizedDiff < 180.0f) ? normalizedDiff : 360.0f - normalizedDiff;
    }

    public void changeBearing(float bearing, float tilt) {
        streetViewPanorama.animateTo(
                new StreetViewPanoramaCamera.Builder()
                    .zoom(streetViewPanorama.getPanoramaCamera().zoom)
                    .tilt(tilt)
                    .bearing(bearing)
                    .build(), 0);

        if(bearing > (220 - 10) && bearing < (220 + 10)) {
            panoWidgetView.setVrModeButtonEnabled(true);
        } else {
            panoWidgetView.setVrModeButtonEnabled(false);
        }
    }

    public void setWalkEnabled(boolean b) {
        walkEnabled = b;
    }

    public synchronized boolean getIsMove() {
        boolean result = isMove;
        isMove = false;
        return result;
    }

    public String getNextPanoID() {
        return nextPanoID;
    }

    public boolean getRunMainProcess() {
        return runMainProcess;
    }

    private class ActivityEventListener extends VrPanoramaEventListener {
        @Override
        public void onLoadSuccess() {
        }

        @Override
        public void onLoadError(String errorMessage) {
            Toast.makeText(MainActivity.this, "Error loading pano: " + errorMessage, Toast.LENGTH_LONG).show();
            Log.e("PANO", "Error loading pano: " + errorMessage);
        }
    }
}


